package ru.example.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.example.controller.EmployeeApi;
import ru.example.usemodel.EmployeeItem;

import java.util.List;

@RestController
public class EmployeeController implements EmployeeApi {
    @Override
    public ResponseEntity<Void> addEmployee(EmployeeItem body) {
        return EmployeeApi.super.addEmployee(body);
    }

    @Override
    public ResponseEntity<List<EmployeeItem>> getEmployee(Integer maxrecord) {
        return EmployeeApi.super.getEmployee(maxrecord);
    }
}
